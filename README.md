UPS(uninterrupted power supply) HAT

CONCEPT
In the event of power outages, the Pi Zero’s SD card can be damaged if the supply power is cut randomly and repeatedly. The purpose of this microHAT is to ensure that the Pi Zero will stay powered long enough so that a user can enable a safe shut down during a power cut to ensure no risk of data loss or system damage. The HAT detects if there is a power failure and automatically switches to the battery source. The Pi Zero is connected to a USB port on the UPS. The UPS is connected directly to the GPIO pins. The UPS communicates via the GPIO port of the Pi Zero. The UPS provides an LED indicator which shows if the battery is full (above 80%) or indicating low voltage. The HAT also has a Voltmeter that will lead to the signal line between 0-3.3V so that
we know how much battery power is left.

Bill of Materials:
4 x Light Emitting Diodes
1 x Zener Diode
4 x Diodes
1 x op - amp
3 x Capacitors
2 x inductors
1 x 40 pin GPIO Header
11 x Resistors
2 x transistors
1 x 12V Battery

REQUIREMENTS

Home security
1. R1.1: Able to keep home security alarms powered during a blackout, especially those
systems that are most vulnerable during power outages.
2. R1.2: Able to stay switched on when power cuts for continuous surveillance.
3. R1.3: Needs to back-up the surveillance tapes during a power outage since there will be
no remote computers working nor any internet wireless connections available.

Arduino
1. R2.1: Constantly switching between different power supplies. Either external supply or battery
supply.
2. R2.2: UPS reports error codes when the battery falls below a specific voltage. One of the status
LEDs will indicate the level of seriousness of the errors.
3. R2.3: The UPS needs to ensure that the Arduino switched off with ease during power outages to
prevent any damage.
4. R2.4: Needs to be a compatible size with the Raspberry PiZero in the Arduino

Household
1. R3.1: Should provide power to the home printer if there's a power outage while the
printer is operating.
2. R3.2: Home media server must be able to provide services to other devices in the
residence.
3. R3.3: Should protect sensitive home electronics from sharp variations in the voltage
supplied to them.

Mechanical Specifications
R2.1 Switching between power supplies
S1.1 Need to use lithium batteries
R2.4 Needs to be compatible size with Rasberry PiZero in Arduino
S1.2 Size needs to be 66.00mm x 30.5mm x 5.0mm .
R3.1 Should provide power to printer during a power outage
S1.3 Needs to have a mini USB/HDMI port.

Electrical Specification
Requirement for 0V – 3.3 V 
S 2.1 Need to use a IC 741 op-amp to restrain voltage.
All components selected are rated for operation within given range.
Final design is chamber tested for 10hrs operation at -40, 0, and 50C.
R1.1 A switching power regulator
S2.2 A zener diode regulator circuit will be used to regulate the voltage to output 5V.
R2.2 Status LEDs used to check voltage value
S2.3 Use standard on-line LEDs to switch on when PiHAT is powering Pi. Operating Temperature: - 40℃~85℃
R2.1: Status LEDs Battery level indicator LEDs with charging/discharging status.

Functional Specifications
R5.4 Device provides audible feedback.
S4.1 Use in-house speaker ID754X1
S4.2 Use 100W or higher amplifier.
S4.3 Feedback must be audible over 250m.
S4.1 - 4.2 Device specifications within range
S4.3: Pass SANSB audio compliance test AB789
Compatible with Raspberry Pi Zero

HOW TO USE 
There isn't a specific procedure. You just plug your PC's and your screen's power cable into the UPS, then plug in the UPS, turn it on, turn your computer on and you are ready to go.

ROLES
The team consists of Gregory Da Silva(Status LEDs), Simone Ndlovu (Voltage regulator) and Alison Adams(Amplifier).

Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

Please note we have a code of conduct, please follow it in all your interactions with the project.

Pull Request Process
Ensure any install or build dependencies are removed before the end of the layer when doing a build.
Update the README.md with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters.
Increase the version numbers in any examples files and the README.md to the new version that this Pull Request would represent. The versioning scheme we use is SemVer.
You may merge the Pull Request in once you have the sign-off of two other developers, or if you do not have permission to do that, you may request the second reviewer to merge it for you.

Code of Conduct
Our Pledge
In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.

Our Standards
Examples of behavior that contributes to creating a positive environment include:

Using welcoming and inclusive language
Being respectful of differing viewpoints and experiences
Gracefully accepting constructive criticism
Focusing on what is best for the community
Showing empathy towards other community members
Examples of unacceptable behavior by participants include:

The use of sexualized language or imagery and unwelcome sexual attention or advances
Trolling, insulting/derogatory comments, and personal or political attacks
Public or private harassment
Publishing others' private information, such as a physical or electronic address, without explicit permission
Other conduct which could reasonably be considered inappropriate in a professional setting
